package ru.t1.gorodtsova.tm.component;

import ru.t1.gorodtsova.tm.api.ICommandController;
import ru.t1.gorodtsova.tm.api.ICommandRepository;
import ru.t1.gorodtsova.tm.api.ICommandService;
import ru.t1.gorodtsova.tm.constant.ArgumentConst;
import ru.t1.gorodtsova.tm.constant.CommandConst;
import ru.t1.gorodtsova.tm.controller.CommandController;
import ru.t1.gorodtsova.tm.repository.CommandRepository;
import ru.t1.gorodtsova.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        processArguments(args);
        System.out.println("** Welcome task manager ** ");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
        exit();
    }

    public void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

    public void exit() {
        System.exit(0);
    }

}
